﻿using UnityEngine;
using System.Collections;

public class LightSystem : MonoBehaviour
{
    public float offIntencity = 0, 
                 lowIntencity = 0.5f, 
                 normalIntensity = 1, 
                 highIntensity = 2;

    public enum LightState
    {
        Off,
        Low,
        Normal,
        High
    }
    public LightState State;

    private Light _light;

    void Start()
    {
        _light = GetComponent<Light>();
        SetIntensity();
    }

    public void Toggle()
    {
        switch (State)
        {
            case LightState.Off:
                SetState(LightState.Low);
                break;
            case LightState.Low:
                SetState(LightState.Normal);
                break;
            case LightState.Normal:
                SetState(LightState.High);
                break;
            case LightState.High:
                SetState(LightState.Off);
                break;
        }
    }

    public void SetState(LightState state)
    {
        State = state;
        SetIntensity();
    }

    void SetIntensity()
    {
        if(Time.timeSinceLevelLoad > 1)
            GetComponent<AudioSource>().Play();

        switch (State)
        {
            case LightState.Off:
                _light.intensity = offIntencity;
                break;
            case LightState.Low:
                _light.intensity = lowIntencity;
                break;
            case LightState.Normal:
                _light.intensity = normalIntensity;
                break;
            case LightState.High:
                _light.intensity = highIntensity;
                break;
        }
    }
}
