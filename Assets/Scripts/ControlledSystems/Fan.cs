﻿using UnityEngine;
using System.Collections;

public class Fan : MonoBehaviour
{
    public float lowSpeed = 5;
    public float normalSpeed = 10;
    public float highSpeed = 15;
    public AudioClip clickSound;

    public enum FanState
    {
        Off,
        Low,
        Normal,
        High
    }

    public FanState State;

    private float _currentSpeed;

    void Start()
    {
        SetSpeed();
    }

    public void Toggle()
    {
        switch (State)
        {
            case FanState.Off:
                SetState(FanState.Low);
                break;
            case FanState.Low:
                SetState(FanState.Normal);
                break;
            case FanState.Normal:
                SetState(FanState.High);
                break;
            case FanState.High:
                SetState(FanState.Off);
                break;
        }
    }

    public void SetState(FanState state)
    {
        State = state;
        SetSpeed();
    }

    void SetSpeed()
    {
        if(Time.timeSinceLevelLoad > 1)
            GetComponent<AudioSource>().PlayOneShot(clickSound);

        if (State != FanState.Off)
        {
            if(!GetComponent<AudioSource>().isPlaying)
                GetComponent<AudioSource>().Play();
        }
        else
            GetComponent<AudioSource>().Stop();

        switch (State)
        {
            case FanState.Off:
                _currentSpeed = 0;
                break;
            case FanState.Low:
                _currentSpeed = lowSpeed;
                break;
            case FanState.Normal:
                _currentSpeed = normalSpeed;
                break;
            case FanState.High:
                _currentSpeed = highSpeed;
                break;
        }
    }

    void Update()
    {
        if (State != FanState.Off)
        {
            transform.Rotate(Vector3.forward, _currentSpeed * Time.deltaTime);
        }   
    }
}
