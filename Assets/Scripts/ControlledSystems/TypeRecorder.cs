﻿using UnityEngine;
using System.Collections;

public class TypeRecorder : MonoBehaviour
{
    public AudioClip track1, track2, track3;

    public enum TypeRecorderState
    {
        Off, 
        Track1, 
        Track2,
        Track3
    }

    public TypeRecorderState State;

    private AudioSource _audioSource;

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        SetTrack();
    }

    public void Toggle()
    {
        switch (State)
        {
            case TypeRecorderState.Off:
                SetState(TypeRecorderState.Track1);
                break;
            case TypeRecorderState.Track1:
                SetState(TypeRecorderState.Track2);
                break;
            case TypeRecorderState.Track2:
                SetState(TypeRecorderState.Track3);
                break;
            case TypeRecorderState.Track3:
                SetState(TypeRecorderState.Off);
                break;
        }
    }

    public void SetState(TypeRecorderState state)
    {
        State = state;
        SetTrack();
    }

    void SetTrack()
    {
        _audioSource.Stop();
        switch (State)
        {
            case TypeRecorderState.Track1:
                _audioSource.PlayOneShot(track1);
                break;
            case TypeRecorderState.Track2:
                _audioSource.PlayOneShot(track2);
                break;
            case TypeRecorderState.Track3:
                _audioSource.PlayOneShot(track3);
                break;
        }
    }
}
