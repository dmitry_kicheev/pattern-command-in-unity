﻿using UnityEngine;
using System.Collections;

public class Guitar : MonoBehaviour
{
    public AudioClip sound1, sound2;

    public enum GuitarState
    {
        Sound1,
        Sound2
    }

    public GuitarState State;

    private AudioSource _audioSource;

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void Touch()
    {
        switch (State)
        {
            case GuitarState.Sound1:
                SetState(GuitarState.Sound2);
                break;
            case GuitarState.Sound2:
                SetState(GuitarState.Sound1);
                break;
        }
    }

    public void SetState(GuitarState state)
    {
        State = state;
        PlaySound();
    }

    void PlaySound()
    {
        _audioSource.Stop();
        switch (State)
        {
            case GuitarState.Sound1:
                _audioSource.PlayOneShot(sound1);
                break;
            case GuitarState.Sound2:
                _audioSource.PlayOneShot(sound2);
                break;
        }
    }
}
