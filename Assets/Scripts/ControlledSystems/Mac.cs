﻿using UnityEngine;
using System.Collections;

public class Mac : MonoBehaviour
{
    public Material offMaterial, desctopMaterial, vkMaterial, vsMaterial, unityMaterial;

    public enum MacState
    {
        Off,
        Desctop,
        Vk,
        Vs,
        Unity
    }

    public MacState State;

    public void Toggle()
    {
        switch (State)
        {
            case MacState.Off:
                SetState(MacState.Desctop);
                break;
            case MacState.Desctop:
                SetState(MacState.Vk);
                break;
            case MacState.Vk:
                SetState(MacState.Vs);
                break;
            case MacState.Vs:
                SetState(MacState.Unity);
                break;
            case MacState.Unity:
                SetState(MacState.Off);
                break;
        }
    }

    public void SetState(MacState state)
    {
        State = state;
        SetScreen();
    }

    void SetScreen()
    {
        GetComponent<AudioSource>().Play();
        Material[] materials = GetComponent<MeshRenderer>().materials;
        switch (State)
        {
            case MacState.Off:
                materials[4] = offMaterial;
                break;
            case MacState.Desctop:
                materials[4] = desctopMaterial;
                break;
            case MacState.Vk:
                materials[4] = vkMaterial;
                break;
            case MacState.Vs:
                materials[4] = vsMaterial;
                break;
            case MacState.Unity:
                materials[4] = unityMaterial;
                break;
        }

        GetComponent<MeshRenderer>().materials = materials;
    }
}
