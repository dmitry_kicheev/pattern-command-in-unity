﻿using UnityEngine;
using System.Collections;

public class PartySphere : MonoBehaviour
{
    public enum PartyState
    {
        Off,
        On
    }

    public PartyState State;

    public void Toggle()
    {
        switch (State)
        {
            case PartyState.Off:
                SetState(PartyState.On);
                break;
            case PartyState.On:
                SetState(PartyState.Off);
                break;
        }
    }

    public void SetState(PartyState state)
    {
        State = state;
        SetParty();
    }

    void SetParty()
    {
        switch (State)
        {
            case PartyState.Off:
                GetComponent<Animator>().SetTrigger("stop");
                break;
            case PartyState.On:
                GetComponent<Animator>().SetTrigger("start");
                break;
        }
    }
}
