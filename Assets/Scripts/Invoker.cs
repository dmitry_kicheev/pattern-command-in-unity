﻿using UnityEngine;
using System.Collections;

public class Invoker : MonoBehaviour
{
    public GameObject partyCanvas;
    private bool _partyOn;

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            if (hit.collider.tag == "Light")
            {
                if(Input.GetMouseButtonDown(0))
                    hit.collider.GetComponent<LightCommand>().Execute();
                if (Input.GetMouseButtonDown(1))
                    hit.collider.GetComponent<LightCommand>().Undo();
            }

            else if (hit.collider.tag == "TypeRecorder")
            {
                if (Input.GetMouseButtonDown(0))
                    hit.collider.GetComponent<TypeRecorderCommand>().Execute();
                if (Input.GetMouseButtonDown(1))
                    hit.collider.GetComponent<TypeRecorderCommand>().Undo();
            }

            else if (hit.collider.tag == "Guitar")
            {
                if (Input.GetMouseButtonDown(0))
                    hit.collider.GetComponent<GuitarCommand>().Execute();
                if (Input.GetMouseButtonDown(1))
                    hit.collider.GetComponent<GuitarCommand>().Undo();
            }

            else if (hit.collider.tag == "Mac")
            {
                if (Input.GetMouseButtonDown(0))
                    hit.collider.GetComponent<MacCommand>().Execute();
                if (Input.GetMouseButtonDown(1))
                    hit.collider.GetComponent<MacCommand>().Undo();
            }

            else if (hit.collider.tag == "Fan")
            {
                if (Input.GetMouseButtonDown(0))
                    hit.collider.GetComponent<FanCommand>().Execute();
                if (Input.GetMouseButtonDown(1))
                    hit.collider.GetComponent<FanCommand>().Undo();
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            partyCanvas.SetActive(!partyCanvas.activeSelf);
        }
    }

    public void OnClick_Party()
    {
        if (!_partyOn)
        {
            partyCanvas.GetComponentInChildren<UnityEngine.UI.Text>().text = "end party";
            GetComponent<PartyMacroCommand>().Execute();
            _partyOn = true;
        }
        else
        {
            partyCanvas.GetComponentInChildren<UnityEngine.UI.Text>().text = "start party";
            GetComponent<PartyMacroCommand>().Undo();
        }
        partyCanvas.SetActive(false);
    }
}
