﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightCommand : MonoBehaviour, ICommand
{
    private Stack<LightSystem.LightState> _states = new Stack<LightSystem.LightState>();
    private LightSystem _lightSystem;

    void Start()
    {
        _lightSystem = GetComponent<LightSystem>();
    }

    public void Execute()
    {
        _lightSystem.Toggle();
        _states.Push(_lightSystem.State);
    }

    public void Undo()
    {
        if (_states.Count > 0)
        { 
            var state = _states.Pop();
            _lightSystem.SetState(state);
        }
    }
}
