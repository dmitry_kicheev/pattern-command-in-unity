﻿using UnityEngine;
using System.Collections;

public class PartyMacroCommand : MonoBehaviour, ICommand
{
    public LightSystem[] lights;
    public Mac mac;
    public Fan fan;
    public TypeRecorder typeRecorder;
    public PartySphere partySphere;

    public void Execute()
    {
        for (int i = 0; i < lights.Length; i++)
        {
            lights[i].SetState(LightSystem.LightState.Low);
        }
        mac.SetState(Mac.MacState.Vk);
        fan.SetState(Fan.FanState.High);
        typeRecorder.SetState(TypeRecorder.TypeRecorderState.Track1);
        partySphere.SetState(PartySphere.PartyState.On);
    }

    public void Undo()
    {
        for (int i = 0; i < lights.Length; i++)
        {
            lights[i].SetState(LightSystem.LightState.Off);
        }
        mac.SetState(Mac.MacState.Off);
        fan.SetState(Fan.FanState.Off);
        typeRecorder.SetState(TypeRecorder.TypeRecorderState.Off);
        partySphere.SetState(PartySphere.PartyState.Off);
    }
}
