﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GuitarCommand : MonoBehaviour, ICommand
{
    private Stack<Guitar.GuitarState> _states = new Stack<Guitar.GuitarState>();
    private Guitar _guitar;

    void Start()
    {
        _guitar = GetComponent<Guitar>();
    }

    public void Execute()
    {
        _guitar.Touch();
        _states.Push(_guitar.State);
    }

    public void Undo()
    {
        if (_states.Count > 0)
        {
            var state = _states.Pop();
            _guitar.SetState(state);
        }
    }
}
