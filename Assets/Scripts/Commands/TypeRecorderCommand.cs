﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TypeRecorderCommand : MonoBehaviour, ICommand
{
    private Stack<TypeRecorder.TypeRecorderState> _states = new Stack<TypeRecorder.TypeRecorderState>();
    private TypeRecorder _typeRecorder;

    void Start()
    {
        _typeRecorder = GetComponent<TypeRecorder>();
    }

    public void Execute()
    {
        _typeRecorder.Toggle();
        _states.Push(_typeRecorder.State);
    }

    public void Undo()
    {
        if (_states.Count > 0)
        {
            var state = _states.Pop();
            _typeRecorder.SetState(state);
        }
    }
}
