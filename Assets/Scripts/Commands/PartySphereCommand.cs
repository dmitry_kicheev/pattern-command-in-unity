﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PartySphereCommand : MonoBehaviour, ICommand
{
    private Stack<PartySphere.PartyState> _states = new Stack<PartySphere.PartyState>();
    private PartySphere _partySphere;

    public void Execute()
    {
        _partySphere.Toggle();
        _states.Push(_partySphere.State);
    }

    public void Undo()
    {
        if (_states.Count > 0)
        {
            var state = _states.Pop();
            _partySphere.SetState(state);
        }
    }
}
