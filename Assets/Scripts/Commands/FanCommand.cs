﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FanCommand : MonoBehaviour, ICommand
{
    private Stack<Fan.FanState> _states = new Stack<Fan.FanState>();
    private Fan _fan;

    void Start()
    {
        _fan = GetComponent<Fan>();
    }

    public void Execute()
    {
        _fan.Toggle();
        _states.Push(_fan.State);
    }

    public void Undo()
    {
        if (_states.Count > 0)
        {
            var state = _states.Pop();
            _fan.SetState(state);
        }
    }
}
