﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MacCommand : MonoBehaviour, ICommand
{
    private Stack<Mac.MacState> _states = new Stack<Mac.MacState>();
    private Mac _mac;

    void Start()
    {
        _mac = GetComponent<Mac>();
    }

    public void Execute()
    {
        _mac.Toggle();
        _states.Push(_mac.State);
    }

    public void Undo()
    {
        if (_states.Count > 0)
        {
            var state = _states.Pop();
            _mac.SetState(state);
        }    
    }
}
